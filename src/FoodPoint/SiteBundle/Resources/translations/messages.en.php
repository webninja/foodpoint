<?php

return array(
    'shop' => array(
        'category' => array(
            'view_quick_list' => array(
                'products' => array(
                    'heading' => array(
                        'stock' => 'Stock Level',
                    ),
                ),
            ),
        ),
        'product' => array(
            'list_glance' => array(
                'heading' => array(
                    'stock' => 'Stock Level',
                ),
            ),
        ),
    ),
);