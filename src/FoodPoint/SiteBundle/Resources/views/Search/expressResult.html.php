<?php //added stock level into express order; ?>
<?php if (is_object($product)): ?>
    <tr>
        <td class="model"><a href="<?php echo $view->escape($view['router']->generate('shop_product_view', array('id' => $product->getId()))); ?>"><?php echo $view->escape($view['product']->getModel($product)); ?></a></td>
        <td class="name"><a href="<?php echo $view->escape($view['router']->generate('shop_product_view', array('id' => $product->getId()))); ?>"><?php echo $view->escape($product->getName()); ?></a></td>

        <?php if ($loginForPrice): ?>
            <td class="login"><a href="<?php echo $view['router']->generate('shop_public_login'); ?>"><?php echo $view->escape($view['translator']->trans('shop.category.view_quick_list.products.product.login')); ?></a></td>
        <?php else: ?>
            <td class="price">
                <?php if ($product->isSpecial()) : ?>
                    <div class="price retail"><?php echo $view['product']->getDisplayPriceForCurrentUser($product, $view['product']->getRetailPrice($product, true)); ?></div>
                    <div class="price special"><?php echo $view['product']->getDisplayPriceForCurrentUser($product, $view['product']->getPrice($product, $product->getMinimumQuantity(), true)); ?></div>
                <?php else : ?>
                    <div class="price"><?php echo $view['product']->getDisplayPriceForCurrentUser($product, $view['product']->getPrice($product, $product->getMinimumQuantity(), true)); ?></div>
                <?php endif; ?>
            </td>
        <?php endif; ?>

        <td class="stock">
            <?php if ($view['product']->canDisplayStockLevelForCurrentUser($product)): ?>
                <div class="value <?php echo $view->escape($view['product']->getStockLevel($product, true)); ?>"><?php echo $view->escape($view['product']->getStockLevel($product)); ?></div>
            <?php endif; ?>
        </td>

        <td class="quantity"><input type="<?php echo $view['product']->getQuantityFieldType($product); ?>" class="quantity" name="quantity[<?php echo $view->escape($product->getId()); ?>]" value="<?php echo $view->escape($quantity); ?>" data-stock-quantity="<?php echo $view->escape($product->getQuantity()); ?>"/></td>
    </tr>
<?php else: ?>
    <tr>
        <td class="error" colspan="<?php echo ($view['product']->canDisplayStockLevelInListForCurrentUser()) ? '5' : '4'; ?>">An error occurred when adding the Product to the list. Please try again.</td>
    </tr>
<?php endif; ?>