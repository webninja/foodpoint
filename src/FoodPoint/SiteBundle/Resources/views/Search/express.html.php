<?php //added stock level into express order; ?>
<?php $view->extend('ShopBundle:Search:express.html.php', false); ?>

<?php $view['slots']->start('results-holder'); ?>
    <form class="cart" method="post" action="<?php echo $view['router']->generate('shop_search_express'); ?>">
        <table class="results table" id="express_results">
            <tr>
                <th class="model">Code</th>
                <th class="name">Name</th>
                <th class="price">Price</th>
                <th class="stock"><?php echo $view->escape($view['translator']->trans('shop.category.view_quick_list.products.heading.stock')); ?></th>
                <th class="quantity">Quantity</th>
            </tr>
            <?php foreach ($productsToDisplay as $productToDisplay): ?>
                <?php echo $view->render('ShopBundle:Search:expressResult.html.php', array('product' => $productToDisplay['product'], 'quantity' => $productToDisplay['quantity'], 'loginForPrice' => $loginForPrice)); ?>
            <?php endforeach; ?>
        </table>
        <?php if (!$loginForPrice): ?>
            <?php if ($view['slots']->has('actions')): ?>
                <?php $view['slots']->output('actions'); ?>
            <?php else: ?>
                <div class="button-outer submit"><input class="button btn btn-primary" type="submit" value="Add to Cart"/></div>
            <?php endif; ?>
        <?php endif; ?>
    </form>
<?php $view['slots']->stop(); ?>