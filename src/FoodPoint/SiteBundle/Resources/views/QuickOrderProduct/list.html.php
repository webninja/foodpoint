<?php $view->extend('QuickOrderBundle:Product:list.html.php', false); ?>

<?php $view['slots']->start('header_cells'); ?>
    <th class="stock"><?php echo $view->escape($view['translator']->trans('shop.category.view_quick_list.products.heading.stock')); ?></th>
<?php $view['slots']->stop(); ?>