<?php $view->extend('ShopBundle:Product:list.html.php', false); ?>

<?php $view['slots']->start('product_headings'); ?>
    <tr style="<?php echo ((is_object($category) && $category->hasBackgroundColour()) ? 'background-color:' . $category->getBackgroundColour() . ';' : ''); ?><?php echo ((is_object($category) && $category->hasForegroundColour()) ? 'color:' . $category->getForegroundColour() . ';' : ''); ?>">
        <th class="code"><?php echo $view->escape($view['translator']->trans('shop.category.view_quick_list.products.heading.code')); ?></th>
        <th class="photo"><?php echo $view->escape($view['translator']->trans('shop.category.view_quick_list.products.heading.image')); ?></th>
        <th class="name"><?php echo $view->escape($view['translator']->trans('shop.category.view_quick_list.products.heading.name')); ?></th>
        <?php if ($view['product']->loginForPriceForCurrentUser()): ?>
            <th class="login">&nbsp;</th>
        <?php else: ?>
            <th class="stock"><?php echo $view->escape($view['translator']->trans('shop.category.view_quick_list.products.heading.stock')); ?></th>
            <th class="price"><?php echo $view->escape($view['translator']->trans('shop.category.view_quick_list.products.heading.price')); ?></th>
            <th class="quantity"><?php echo $view->escape($view['translator']->trans('shop.category.view_quick_list.products.heading.quantity')); ?></th>
        <?php endif; ?>
    </tr>
<?php $view['slots']->stop(); ?>