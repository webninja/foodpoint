<tr class="product <?php echo $view['product']->getFlags($product); ?>">
    <td class="code"><?php echo $view->escape($view['product']->getModel($product)); ?></td>
    <td class="photo">
        <?php if ($product->hasImage()) : ?>
            <a href="#image-popup-<?php echo $view->escape($product->getId()); ?>" role="button" class="photo-thumbnail" data-toggle="modal"><img src="<?php echo $view['product']->getImageUrl($product, 'shop.product.list_glance.photos.thumbnail'); ?>"/></a>
            <div id="image-popup-<?php echo $view->escape($product->getId()); ?>" class="modal hide fade lightbox" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <img src="<?php echo $view['product']->getImageUrl($product, 'shop.product.list_glance.photos.photo'); ?>"/>
                </div>
            </div>
        <?php endif; ?>
    </td>
    <td class="name"><a href="<?php echo $view['router']->generate('shop_product_view', array('product' => $product)); ?>"><?php echo $view->escape($product->getName()); ?></a></td>
    <?php if (!$view['product']->canDisplayPriceForCurrentUser($product)) : ?>
        <td class="login"><a href="<?php echo $view['router']->generate('shop_public_login'); ?>"><?php echo $view->escape($view['translator']->trans('shop.product.list_glance.login')); ?></a></td>
    <?php else : ?>
        <td class="stock">
            <div class="value <?php echo $view->escape($view['product']->getStockLevel($product, true)); ?>"><?php echo $view->escape($view['product']->getStockLevel($product)); ?></div>
        </td>
        <td class="price">
            <?php if ($product->isSpecial()) : ?>
                <div class="price retail"><?php echo $view['product']->getDisplayPriceForCurrentUser($product, $view['product']->getWasPrice($product, true)); ?></div>
                <div class="price special"><?php echo $view['product']->getDisplayPriceForCurrentUser($product, $view['product']->getPrice($product, $product->getMinimumQuantity(), true)); ?></div>
            <?php else : ?>
                <div class="price"><?php echo $view['product']->getDisplayPriceForCurrentUser($product, $view['product']->getPrice($product, $product->getMinimumQuantity(), true)); ?></div>
            <?php endif; ?>
        </td>

        <td class="quantity">
            <?php if ($view['product']->allowBuy($product)): ?>
                <input class="quantity" type="<?php echo $view['product']->getQuantityFieldType($product); ?>" name="quantity[<?php echo $view->escape($product->getId()); ?>]" value="" data-stock-quantity="<?php echo $view->escape($product->getQuantity()); ?>" <?php if (isset($index)): ?>tabindex="<?php echo $view->escape(++$index); ?>"<?php endif; ?>/>
            <?php else: ?>
                <?php if ($view['product']->allowInStockNotification($product) && $view['product']->allowInStockNotificationOnListGlance($product)): ?>
                    <div class="notify">
                        <div class="button-outer notify"><a class="button btn" href="<?php echo $view['router']->generate('shop_product_notify', array('product' => $product)); ?>"><?php echo $view['translator']->trans('shop.product.list_glance.notify.notify'); ?></a></div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </td>
    <?php endif; ?>
</tr>