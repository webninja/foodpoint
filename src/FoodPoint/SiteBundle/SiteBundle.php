<?php

class SiteBundle_SiteBundle extends Webninja_BaseBundle
{
    public function boot()
    {
        $container = $this->getContainer();
        $entityManager = $container->get('doctrine')->getManager();
        $templating = $container->get('templating');

        $templating->addOverride('ShopBundle:Search:resultsQuickList.html.php', 'SiteBundle:Search:resultsQuickList.html.php');
        $templating->addOverride('ShopBundle:Product:list.html.php', 'SiteBundle:Product:list.html.php');
        $templating->addOverride('ShopBundle:Product:listGlance.html.php', 'SiteBundle:Product:listGlance.html.php');
        $templating->addOverride('QuickOrderBundle:Product:list.html.php', 'SiteBundle:QuickOrderProduct:list.html.php');
        $templating->addOverride('ShopBundle:Search:express.html.php', 'SiteBundle:Search:express.html.php');
        $templating->addOverride('ShopBundle:Search:expressResult.html.php', 'SiteBundle:Search:expressResult.html.php');
    }

    protected function getPath()
    {
        return dirname(__FILE__) . '/../';
    }
}
