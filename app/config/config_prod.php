<?php

$config = array(
    'HTTP_SERVER'                  => 'http://foodpoint.webninjashops.com',
    'HTTPS_SERVER'                 => 'https://foodpoint.webninjashops.com',
    'HTTP_COOKIE_DOMAIN'           => 'foodpoint.webninjashops.com',
    'HTTPS_COOKIE_DOMAIN'          => 'foodpoint.webninjashops.com',

    'DIR_FS_CATALOG'               => '/var/www/sites/foodpoint/',

    'DB_SERVER_USERNAME'           => 'foodpoint',
    'DB_SERVER_PASSWORD'           => 'e5bhmq7w',

    'CMS_BASE_DIRECTORY'           => '/var/www/cms/',
    'CORELIB_BASE_DIRECTORY'       => '/var/www/corelib/',
);

foreach ($config as $key => $value) {
    if (!defined($key)) {
        define($key, $value);
    }
}
